require "Keller"
require "Element"

# ================
# = Vorbereitung =
# ================

# Methode, die einen zufälligen Zug mit Länge n erzeugt.
def random_train(n)
  s = Stack.new
  (1..n).to_a.shuffle.each { |e| s.push e }
  return s
end

def rangieren1(r1)
  train = Stack.new
  r2 = Stack.new
  gesucht = 1
  
  #bis beide Gleise leer sind
  until r1.empty?
    until r1.empty?
      puts train.show
      puts r1.show
      puts r2.show
      
      waggon = r1.pop
    
      if waggon == gesucht
        puts "Wagen #{gesucht} an Zug angehängt."
        train.push waggon
        gesucht += 1
      else
        puts "Schiebe Wagen #{waggon} auf's Rangiergleis."
        r2.push waggon
      end
    end
  
    r1, r2 = r2, r1
  end
  
  return train
end

# 1. Gucken, auf welchem Gleis mehr Wagen stehen => Stack erweitern um eine Methode size, die die Anzahl eingekellerter Elemente zurück
# nach Z. 47: r1, r2 = r2, r1 if r2.size > r1.size
def rangieren2(r1)
  train = Stack.new
  r2 = Stack.new
  gesucht = 1
  
  # bis beide Gleise leer sind
  until r1.empty?
    until r1.empty?
      waggon = r1.pop
    
      if waggon == gesucht
        train.push waggon
        gesucht += 1
        r1, r2 = r2, r1 if r2.size > r1.size
      else
        r2.push waggon
      end
    end
  
    r1, r2 = r2, r1
  end
end


# 2. Stack beibringen, nach einem bestimmten Element zu suchen "enthalten?(element)"
# nach Z. 47: r1, r2 = r2, r1 if r2.include?(gesucht)
def rangieren3(r1)
  gesucht = 1
  
  #bis beide Gleise leer sind
  until r1.empty?
    until r1.empty?
      waggon = r1.pop
    
      if waggon == gesucht
        train.push waggon
        gesucht += 1
        r1, r2 = r2, r1 if r2.include?(gesucht)
      else
        r2.push waggon
      end
    end
  
    r1, r2 = r2, r1
  end
end

random_train = random_train(10)
rangieren1(random_train.dup)
rangieren2(random_train.dup)
rangieren3(random_train.dup)
