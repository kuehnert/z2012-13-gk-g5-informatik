require "Schlange"

class Liste < Schlange
  # kann alles, was Schlange kann

  def groesse
    if leer?
      return 0
    else
      anzahl  = 1
      laeufer = @erstes
    
      until laeufer.naechstes == nil
        anzahl += 1
        laeufer = laeufer.naechstes
      end
      
      return anzahl
    end
  end
  
  def to_s
    if leer?
      out = "Liste leer"
    else
      index = 0
      laeufer = @erstes
      out = "[\n"
      while laeufer
        out << "  #{index}: #{laeufer.wert}\n"
        index += 1
        laeufer = laeufer.naechstes
      end
      
      return out + "]"
    end
  end
  
  def element_at(zielindex)
    if zielindex < 0
      raise "index #{zielindex} ungültig"
    elsif zielindex >= groesse
      raise "index #{zielindex} zu groß"
    end
    
    laeufer = @erstes
    zielindex.times do
      laeufer = laeufer.naechstes
    end
    
    return laeufer.wert
  end
  
  def index_of(gesucht)
    zaehler = 0
    laeufer = @erstes
    
    until laeufer == nil || laeufer.wert == gesucht
      laeufer = laeufer.naechstes
      zaehler += 1
    end
    
    if laeufer
      return zaehler
    else
      return nil
    end
  end
  
  def contains?(value)
    index_of(value) != nil
  end
  
  # OBLIG
  def einfuegen(wert, index = groesse)
    e = Element.new(wert)
    
    if index > groesse && index < 0
      raise "Ungueltiger Index"
    end
    
    if index == 0
      e.naechstes = @erstes
      @erstes = e
    else
      laeufer = @erstes
      
      index.times do
        laeufer = laeufer.naechstes
      end
      
      laeufer.naechstes = e
    end
    
  end
  
  # - entfernen(index = ERSTES)
  # OPT
  # - replace_at(index, element)

  # - Iterator (weil Weihnachten ist)
end
