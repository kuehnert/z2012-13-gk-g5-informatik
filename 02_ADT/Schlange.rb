require "Element"

class Schlange
  def initialize
    @erstes = nil
  end

  def erstes
    if leer?
      raise "Liste ist leer!"
    else
      return @erstes.wert
    end
  end
  
  def einfuegen(wert)
    e = Element.new(wert)
    
    if leer?
      @erstes = e
    else
      laeufer = @erstes
      
      while laeufer.naechstes != nil
        laeufer = laeufer.naechstes
      end
      
      laeufer.naechstes = e
    end
  end
  
  def entfernen
    if leer?
      raise "Liste ist leer!"
    else
      wert = @erstes.wert
      @erstes = @erstes.naechstes
      return wert
    end
  end
  
  def leeren
    @erstes = nil
  end
  
  def leer?
    return @erstes == nil
  end
end
