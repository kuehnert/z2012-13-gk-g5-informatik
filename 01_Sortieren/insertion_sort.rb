require "./sort_helper"

def insertion_sort!(a)
  1.upto(a.size-1) do |i|
    i.downto(1) do |j|
      if a[j] < a[j-1]
        swap(a, j, j-1)
      else
        break
      end
    end
  end
end

def insertion_sort_html!(a)
  out = "<html><body><tt>"
  1.upto(a.size-1) do |i|
    # out << "<h5>i == #{i}</h5>"
    i.downto(1) do |j|
      out << print_line_html(i, j, a)
      if a[j] < a[j-1]
        swap(a, j, j-1)
        out << print_line_html(i, j, a)
      else
        break
      end
    end
  end

  out << "</tt></body></html>"
  File.open("insertion_sort_example.html", "w") { |io| io.puts out }
  `open insertion_sort_example.html`
end
