# Keller, Stapel, Stack
# FILO-Speicher = First In, Last Out
# Operationen:

# + push(wert: Object): nil (einkellern, oben einfügen)
# + pop(): Object (auskellern, von oben entfernen & zurückgeben)
# + top(): Object (oberstes zurückgeben)
# + clear(): nil (leeren)
# + empty?(): Boolean (leer?)

class Stack
  def initialize
    @top = nil
  end
  
  def push(wert)
    e = Element.new(wert)
    
    if empty?
      @top = e
    else
      e.naechstes = @top
      @top = e
    end
  end
  
  def pop
    if empty?
      raise "Fehlerdingsbums"
    else
      wert = @top.wert      
      @top = @top.naechstes
      return wert
    end
  end
  
  def top
    if empty?
      raise "Würg"
    else
      return @top.wert
    end
  end
  
  def clear
    @top = nil
  end
  
  def empty?
    return @top == nil
  end

  def size
    if empty?
      return 0
    else
      anzahl  = 1
      laeufer = @top
    
      until laeufer.naechstes == nil
        anzahl += 1
        laeufer = laeufer.naechstes
      end
      
      return anzahl
    end
  end
  
  def include?(value)
    runner = @top
    until runner == nil
      if runner.wert == value
        return true
      end
      
      runner = runner.naechstes
    end
    
    return false
  end
  
  def show
    if empty?
      return "[]"
    else
      output = "["
      runner = @top
      until runner == nil
        output += "#{runner.value}-"
        runner = runner.next
      end
      
      return output + "]"
    end
  end
end
