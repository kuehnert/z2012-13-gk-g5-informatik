require "./sort_helper"

# Gehe mit <start> von vorne nach hinten durch das Feld:
#   1. Setze <min> auf den Index des kleinsten Elements
#   2. Vertausche die Elemente an <min> und <start>
#   3. Erhöhe start um 1
#   
# 0. Setze start auf 0
# 
# 1. Setze <min> auf den Index des kleinsten Elements
#   a. setze <aktuell> auf <start> + 1
#   b. wenn Element an <aktuell> < Element an <min>, dann setze <min> auf <aktuell>
#   c. erhöhe <aktuell>
#   d. gehe zu 1. wenn <aktuell> < die Größe von a
# 
# 2. Vertausche die Elemente an <min> und <start>
#   a. Schreibe eine Methode <swap>
#   
# 3. Erhöhe start um 1


# 1. Schreibe das Programm um mit while-Schleifen
# n.b. "0...4" steht für "0..3" also "0", "1", "2", "3"
# 
# 2. Programmiere eine Methode die ein zufälliges Feld mit <n> Elementen erstellt. Messe, wie lange das Sortieren von 100, 200, 300, 400, ..., 1000
# von 1000, 2000, 3000, 4000, 5000 Elementen dauert
# 3. Erstelle einen LibreOffice Graph, der die Zeiten und Anzahlen darstellt.

def min_sort!(a)
  (0...a.size).each do |start|
    min = start
    (start+1).upto(a.size - 1) do |aktuell|
      if a[aktuell] < a[min]
        min = aktuell
      end
    end
    
    swap(a, start, min)
  end
end

def min_sort_while!(a)
  start = 0
  while start < a.size - 1
    min = start
    aktuell = start + 1
    while aktuell < a.size
      if a[aktuell] < a[min]
        min = aktuell
      end
      
      aktuell += 1
    end
    
    swap(a, start, min)
    start += 1
  end
end

def min_sort_count(a)
  @vertauschungen = @vergleiche = 0
  
  (0..(a.size-2)).each do |start|
    min = start
    (start+1).upto(a.size - 1) do |aktuell|
      if a[aktuell] < a[min]
        min = aktuell
      end
      @vergleiche += 1
    end
    
    if start != min
      swap(a, start, min)
      @vertauschungen += 1
    end
    @vergleiche += 1
  end
end

# CSV-Datei: Comma-Separated Values
# output = "Elemente;Vergleiche;Vertauschungen\n"
# 10.times do |i|
#   count = (i+1) * 100
#   a = random_array( count )
#   min_sort_count(a)
#   output << [count, @vergleiche, @vertauschungen].join(";") + "\n"
# end
# 
# File.open("MinSort.csv", "w") { |io| io.write output }
# `open MinSort.csv`
