# encoding: UTF-8

require_relative "binbaum"

class Tierspiel < Node
  def initialize()
    self.value  = "Ist es ein Säugetier?"
    self.left   = Node.new("Hund")
    self.right  = Node.new("Eidechse")
    
    new_game
  end
  
  def new_game
    @weiter = true
    @runner = self
  end
  
  def spielen
    while @weiter do
      if @runner.leaf?
        print "Is it a #{@runner.value}? "
        antwort = gets.chomp
        
        if antwort =~ /j|y/
          puts "Hurrah, I have won! :)"
          @weiter = false
        else
          puts "Oh, I don't know your animal! :("
          # Hier muss noch was hin
          new_game
        end
      else
        puts @runner.value
        antwort = gets.chomp

        if antwort =~ /j|y/
          @runner = @runner.left
        else
          @runner = @runner.right
        end
      end
    end
  end
end

Tierspiel.new.spielen
