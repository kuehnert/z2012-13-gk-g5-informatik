require "binbaum"

def baum_top_down
  baum 													= Binaerbaum.new("L")
  baum.root.left								= Node.new("R")
  baum.root.left.left 					= Node.new("X")
  baum.root.left.right 					= Node.new("S")
  baum.root.right								= Node.new("Z")
  baum.root.right.left					= Node.new("A")
  baum.root.right.left.left 		= Node.new("F")
  baum.root.right.left.right 		= Node.new("E")
  baum.root.right.right					= Node.new("C")
  return baum
end

def baum_bottom_up
  an = Node.new("A", Node.new("F"), Node.new("E"))
  zn = Node.new("Z", an, Node.new("C"))
  rn = Node.new("R", Node.new("X"), Node.new("S"))
  baum = Node.new("L", rn, zn)
  return baum
end

def baum_mazc
  root = Node.new("M")
  root.insert_recursive("A")
  root.insert_recursive("Z")
  root.insert_recursive("C")
  root.insert_recursive("B")
  root.insert_recursive("D")
  root.insert_recursive("H")
  root.insert_recursive("G")
  root.insert_recursive("T")
  root.insert_recursive("J")
  root.insert_recursive("P")
  
  root.ausgabe_lnr
  puts
  
  puts root.find("M")
  puts root.find("A")
  puts root.find("J")
  puts root.find("X")
  
end

baum_mazc
