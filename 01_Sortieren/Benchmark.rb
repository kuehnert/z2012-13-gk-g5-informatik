require "./sort_helper"
require "./min_sort"
require "./bubble_sort"
require "./insertion_sort"

MIN_ELEMENTS = 5000
MAX_ELEMENTS = 5000
STEP         = 1000
# :bubble_sort!,
# :insertion_sort!,
ALGORITHMS   = [:min_sort!,  :bubble_sort_plus!,  :ruby_sort!]
# ALGORITHMS   = [:ruby_sort!]

def ruby_sort!(a)
  a.sort!
end

def benchmark(algorithm, sort_case, n)
  a = send(sort_case, n)
  start = Time.now
  send algorithm, a
  duration = Time.now - start
  print "%.4f\t" % duration
end

puts "Starting Benchmark..."
[:best_case, :average_case, :worst_case].each do |sort_case|
  puts sort_case.upcase
  
  puts (["n"] + ALGORITHMS).join("\t")
  MIN_ELEMENTS.step(MAX_ELEMENTS, STEP) do |n|
    print "#{n}\t"
  
    ALGORITHMS.each do |algorithm|
      benchmark(algorithm, sort_case, n)
    end

    puts
  end
  
  puts
end
puts "Benchmark finished."
