# 
#  sort_helper.rb
#  2012-13-GK-G5-Informatik
#  
#  Created by Matthias Kühnert on 2012-09-20.
#  Copyright 2012 Matthias Kühnert. All rights reserved.
# 
def best_case(n)
  return Array.new(n) { |i| i }
end

def average_case(n)
  return best_case(n).shuffle
end

def worst_case(n)
  return best_case(n).reverse
end

def swap(array, i, j)
  tmp = array[i]
  array[i] = array[j]
  array[j] = tmp
end

def print_line(i, j, a)
  puts ([i, j] + a).map { |e| "%3d|" % e}.join
end

def print_line(i, j, a)
  print "%3d|%3d|" % [i, j]
  a.each { |e| print "%3d|" % e }
  puts
end

def print_header(a)
  puts "  i|  j|" + best_case(a.size).map { |e| "%3d|" % e}.join
  puts "-" * (a.size + 2) * 4
end

def print_line_html(i, j, a)
  if i == j
    a[0...j].join(' ') + html_char(a, i, 'magenta') + a[(j+1)..(a.size-1)].join(' ') + "<br>\n"
  else
    a[0...j].join(' ') + " <font color='red'>#{a[j]}</font> " + a[j+1...i].join(' ') + " <font color='blue'>#{a[i]}</font> " + a[(i+1)..(a.size-1)].join(' ') + "<br>\n"
  end
end

private

def html_char(a, var, colour)
  " <font color='#{colour}'><strong>#{a[var]}</strong></font> "
end
