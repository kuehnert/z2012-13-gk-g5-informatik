#!/usr/bin/env ruby
require "Liste"

l = Liste.new
puts l.leer? # ja
puts l.groesse # 0
puts l

# vererbte Methode
l.einfuegen "Katharina"
l.einfuegen "Bob"
l.einfuegen "Sponge"

puts l.leer? # nein
puts l.groesse # 3
puts l
puts l.element_at(2)
puts l.element_at(1)
puts l.element_at(0)
# puts l.element_at(3) # "zu groß"
# puts l.element_at(-1) # "ungültig"

puts l.index_of("Katharina")
puts l.index_of("Bob")
puts l.index_of("Sponge")
puts l.index_of("Spongebob")

if l.contains?("Bob")
  puts "Bob ist enthalten."
else
  puts "Bob ist NICHT enthalten."
end

if l.contains?("Spongebob")
  puts "Spongebob ist enthalten."
else
  puts "Spongebob ist NICHT enthalten."
end
