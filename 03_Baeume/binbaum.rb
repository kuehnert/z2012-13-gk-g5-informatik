class Binaerbaum
  attr_reader :root
  
  def initialize(value, left = nil, right = nil)
    @root = Node.new(value, left, right)
  end
  
  def ausgabe
    @root.ausgabe
  end
end

class Node
  attr_accessor :left, :right, :value
  
  def initialize(value, left = nil, right = nil)
    @value = value
    @left  = left
    @right = right
  end
  
  # in-order
  def ausgabe_nlr
    print "#@value, "
    
    if @left
      @left.ausgabe_nlr
    end
    
    if @right
      @right.ausgabe_nlr
    end
  end

  # pre-order
  def ausgabe_lnr
    if @left
      @left.ausgabe_lnr
    end

    print "#@value, "
    
    if @right
      @right.ausgabe_lnr
    end
  end

  # post-order
  def ausgabe_lrn
    if @left
      @left.ausgabe_lrn
    end
    
    if @right
      @right.ausgabe_lrn
    end

    print "#@value, "
  end

  def find(value)
    runner = self
    
    until runner == nil
      if value == runner.value
        return runner.value
      elsif value < runner.value
        runner = runner.left
      else # value > runner.value
        runner = runner.right
      end
    end
    
    return nil
  end
  
  def find_recursive(value)
    if value == @value
      return @value
    elsif value < @value && @left
      return @left.find_recursive(value)
    elsif value > @value && @right
      return @right.find_recursive(value)
    end
    
    return nil
  end
  
  def insert_recursive(value)
    if value < self.value
      if @left == nil # elementary
        @left = Node.new(value)
      else # recursive
        @left.insert_recursive(value)
      end
    elsif value > self.value
      if @right == nil
        @right = Node.new(value)
      else
        @right.insert_recursive(value)
      end
    else
      puts "#{value} gibt es schon!"
    end
  end
end
