require "./sort_helper"

def bubble_sort!(a)
  (a.size - 1).downto(1) do |i|
    0.upto(i-1) do |j|
      if a[j] > a[j+1]
        swap(a, j, j+1)
      end
    end
  end
end

def bubble_sort_tab!(a)
  (a.size - 1).downto(1) do |i|
    0.upto(i-1) do |j|
      if a[j] > a[j+1]
        swap(a, j, j+1)
      end
      print_line(i, j, a)
    end
  end
end

def bubble_sort!(a)
  (a.size - 1).downto(1) do |i|
    0.upto(i-1) do |j|
      if a[j] > a[j+1]
        swap(a, j, j+1)
      end
    end
  end
end

def bubble_sort_plus!(a)
  (a.size - 1).downto(1) do |i|
    swaps = 0
    0.upto(i-1) do |j|
      if a[j] > a[j+1]
        swap(a, j, j+1)
        swaps += 1
      end
    end

    break if swaps == 0
  end
end
